package b137.alinabon.s03d1;

public class RepetitionsStatements {

    public static void main(String[] args){
        System.out.println("Repetitions Statements");

        // for statement

        for(int i = 0; i < 5; i++){
            System.out.println(i + 1);
        }
        System.out.println();

        // Mini activity
        // Declare an array of numbers 100 , 200 , 300, 400, 500
        // Use the for statements to display those numbers in the console

        int[] numbersInHundredsList = {100, 200, 300, 400, 500};

        for(int i = 0; i < numbersInHundredsList.length; i++ ){
            System.out.println(numbersInHundredsList[i]);
        }
        System.out.println();

        String[] warriors = {"Curry", "Thompson", "Green", "Wiggins", "Wiseman"};

        for (String warrior : warriors){
            System.out.println(warrior);
        }

        System.out.println();
        // Mini Activity
        // Implement the solution for the previous mini-activity using the enhanced for statement

        for(int number : numbersInHundredsList){
            System.out.println(number);
        }
        System.out.println();

        // while and do while

        int x = 0;
        int y = 10;

        while (x < 10){
            System.out.println("Loop number " + x);
            x = x + 1;
        }

        System.out.println();

        do {
            System.out.println("Countdown: " + y);
            y = y - 1;
        }while(y > 0);
    }
}
